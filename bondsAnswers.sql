-- 1.
select * from bond where CUSIP='28717RH95';
--2.
select * from bond order by maturity asc;
--3.
select sum(quantity*price)as 'Portfolio Value' from bond;
--4
select sum((quantity*coupon)/100)as 'Portfolio Value' from bond group by id;
--5
select * from bond
join rating on bond.rating=rating.rating
where rating.ordinal in(1,2,3)
order by rating.ordinal asc;
--6
select rating, avg(price)as 'average price',avg(coupon) as 'average coupon rate'
from bond
group by rating;
--7
select *
from bond
join rating on bond.rating=rating.rating
where coupon/price <rating.expected_yield;