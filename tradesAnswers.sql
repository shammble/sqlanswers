--1
SELECT t.first_name,t.last_name,opening_trade_id,closing_trade_id, instant,stock,case when tr.buy = 1 then 'BUY' else 'SELL' end,size,price from trader as t
JOIN position as p on t.id=p.trader_id
INNER JOIN trade as tr on opening_trade_id=tr.id or closing_trade_id=tr.id
WHERE t.id=1 and stock='MRK'
ORDER BY tr.instant asc;
--2
SELECT p.trader_id as 'Trader',stock as 'Stock',sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'Profitability' 
from trade as t
join position as p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 2 and t.stock = 'MRK'
and p.closing_trade_id is not null
group by p.trader_id,stock;

--3
create view trader_PL as
select tr.first_name, tr.last_name, sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'P/L'
from trade as t
join position as p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
join trader as tr on p.trader_id = tr.id
where p.closing_trade_id is not null
group by tr.first_name, tr.last_name;

-- 3.1 run after view created
select * from trader_PL

-- 3.2 drop view
drop view trader_PL